
# Civitas Cartrack (In English below)
Una aplicación para Android desarrollada para recoger datos en mi artículo:

"Félix Jesús Villanueva, David Villa, Maria J. Santofimia, Jesús Barba, Juan Carlos López: Crowdsensing smart city parking monitoring. WF-IoT 2015: 751-756"

Puedes consultar los datasets del paper aquí:
https://bitbucket.org/arco_group/crowdsensing-parkingmonitoring-git/src/master/

La aplicación toma la información de diferentes sensores (ubicación, acelerómetro, giroscopio y magnetómetro) y la guarda en un archivo csv guardando también un archivo de metadatos con la orientación del teléfono móvil. 

Actualmente, se está actualizando para etiquetar los datos según comandos de voz para obtener un conjunto de datos de los sensores mientras se conduce.


# Civitas Cartrack

Android app developed to collect data in my article:

"Felix Jesús Villanueva, David Villa, Maria J. Santofimia, Jesús Barba, Juan Carlos López: Crowdsensing smart city parking monitoring. WF-IoT 2015: 751-756"
You can download the dataset from here:
https://bitbucket.org/arco_group/crowdsensing-parkingmonitoring-git/src/master/

The app takes info from different sensors (location, accelerometer, gyroscope and magnetometer) and save it to a csv file saving also a metadata file with the mobile phone orientation. 

Currently, it is being updated to tag data according voice commands to get a dataset of sensors meanwhile driving. 
