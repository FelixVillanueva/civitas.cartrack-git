package com.example.felix.civitascartrack;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class AcellerometersTracking extends Service implements SensorEventListener, LocationListener {

    private SensorManager senSensorManager;
    private Sensor senRot;

    private Location location; // location
    private double latitude; // latitude
    private double longitude; // longitude
    private float x;
    private float y;
    private float z;
    private float gyrox;
    private float gyroy;
    private float gyroz;

    private float magx;
    private float magy;
    private float magz;
    private BufferedWriter buf;
    private BufferedWriter bufmetadata;
    private boolean trackGPS;
    boolean trackAcc;
    boolean trackMag;
    boolean trackVC;
    boolean trackGyro;
    boolean ResetService;

    File logFile;
    File logFilemetadata;
    public String EventClass;

    private String LastAction = "NA";
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1; // 1 meters
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 10;
    final Context context = this;

    @Override
    public void onCreate() {
        senSensorManager = (SensorManager) getSystemService(context.SENSOR_SERVICE);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ResetService = intent.getBooleanExtra("Reset", false);

        if (ResetService) {
            Log.w("Reset invoked", "stopping");
        }

        trackGPS = intent.getBooleanExtra("TrackGPS", false);
        trackAcc = intent.getBooleanExtra("TrackAcc", false);
        trackMag = intent.getBooleanExtra("TrackMag", false);
        trackVC = intent.getBooleanExtra("TrackVC", false);
        trackGyro = intent.getBooleanExtra("TrackGyro", false);

        String filenametext = intent.getStringExtra("FileName");
        createLogFiles(filenametext);
        String commenttext = intent.getStringExtra("Comments");
        writeComment(commenttext);

        if (trackGPS) {
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (isGPSEnabled) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    Log.d("SensorTracking", "Permission granted");
                }
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                Log.d("SensorTracking", "GPS Enabled");
            }else{
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle("GPS is settings").setMessage("GPS is not enabled");
            }
        }

        if (trackAcc) {
            Sensor senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        }

        if (trackMag) {
            Sensor senMag = senSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED);
            senSensorManager.registerListener(this, senMag, SensorManager.SENSOR_DELAY_NORMAL);
        }
        if (trackGyro) {
            Sensor senGyro = senSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
            senSensorManager.registerListener(this, senGyro, SensorManager.SENSOR_DELAY_NORMAL);
        }

        /* metadata save the position of the mobile phone. */
        senRot = senSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        senSensorManager.registerListener(this, senRot, SensorManager.SENSOR_DELAY_NORMAL);
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }

    private void writeComment(String s) {
        try{
            bufmetadata.append(s);
            bufmetadata.newLine();
        }catch (IOException e){
            e.printStackTrace();
        }
    }


    public AcellerometersTracking() {

    }

    private void createLogFiles(String filename){
        String format = "yyyy-MM-dd-HH-mm-ss";
        EventClass=filename;
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File directory = cw.getExternalFilesDir(null);
        String fullfilename=EventClass+"-"+sdf.format(new Date())+".scv";
        String fullfilenamemetadata=EventClass+"-"+sdf.format(new Date())+".metadata";
        logFile = new File(directory, fullfilename);
        logFilemetadata = new File(directory, fullfilenamemetadata);
        if (!logFile.exists())
        {
            try
            {
                if (logFilemetadata.createNewFile())
                {
                    Log.d("logFilemetadata", logFilemetadata.getAbsolutePath());
                }else{
                  Log.e("logFilemetadata", "problem");
                }
                if (logFile.createNewFile()){
                    Log.d("logFile",logFile.getAbsolutePath());
                } else{
                    Log.e("logFile", "problem");
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        try {
            bufmetadata = new BufferedWriter(new FileWriter(logFilemetadata, true));
            buf = new BufferedWriter(new FileWriter(logFile, true));
        }catch(IOException e){
            e.printStackTrace();
        }
        putHeadofLogFile();
    }

    private void putHeadofLogFile(){
        String head="Time";
        if(trackGPS) head=head+", Longitude"+", Latitude";
        if(trackAcc) head=head+", Accx"+", Accy"+", Accz";
        if(trackMag) head=head+", Magx"+", Magy"+", Magz";
        if(trackGyro) head=head+", Gyrox"+", Gyroy"+", Gyroz";
        if(trackVC) head=head+", VoiceCommand";
        try {
            buf.append(head);
            buf.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // GPS part

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        writeTrack();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    public class LocalBinder extends Binder {
        AcellerometersTracking getService() {
            return AcellerometersTracking.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
         return mBinder;
    }
    private final IBinder mBinder = new LocalBinder();

    // SENSORS

    @Override
    public void onSensorChanged(SensorEvent event) {

        Sensor mySensor = event.sensor;
        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            x = event.values[0];
            y = event.values[1];
            z = event.values[2];
            writeTrack();
            return;
        }
        if (mySensor.getType() == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED) {
            magx = event.values[0];
            magy = event.values[1];
            magz = event.values[2];
            writeTrack();
            return;
        }
        if(mySensor.getType() == Sensor.TYPE_GYROSCOPE){
            gyrox = event.values[0];
            gyroy = event.values[1];
            gyroz = event.values[2];
            writeTrack();
            return;
        }
        if (mySensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            float rotationMatrix[];
            rotationMatrix=new float[16];
            senSensorManager.getRotationMatrixFromVector(rotationMatrix,event.values);
            writeMetadata(rotationMatrix);
            senSensorManager.unregisterListener(this, senRot);
            return;
        }

    }

    public void writeMetadata(float rotationMatrix[]){
        Log.i("Matrix ",rotationMatrix.toString());
        try{
            bufmetadata.append("#EventClass: ");
            bufmetadata.append(EventClass.toString());
            bufmetadata.newLine();
            bufmetadata.append("Rotation Matrix:");
            bufmetadata.newLine();
            for (int i = 0; i < rotationMatrix.length; i++){
                bufmetadata.append(rotationMatrix[i] +", ");
            }
            bufmetadata.newLine();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
    public void VoiceActionNotify(int action){
        switch (action){
            case Constants.ACTIVITY_NOT_RECOGNIZED:
                LastAction="Action not recognized";
                break;
            case Constants.ARRANCAR:
                LastAction="Arrancar";
                break;
            case Constants.CARRIL_DERECHA:
                LastAction="Carril Derecha";
                break;
            case Constants.CARRIL_IZQUIERDA:
                LastAction="Carril Izquierda";
                break;
            case Constants.FRENAR:
                LastAction="Frenar";
                break;
            case Constants.RECTO:
                LastAction="Recto";
                break;
            case Constants.STOP:
                LastAction= "Stop";
                break;
            case Constants.ROTONDA:
                LastAction="Rotonda";
                break;
            case Constants.GIRAR_DERECHA:
                LastAction="Girar Derecha";
                break;
            case Constants.GIRAR_IZQUIERDA:
                LastAction="Girar Izquierda";
                break;
            case Constants.COCHE:
                LastAction="Coche";
                break;
            case Constants.LIBRE:
                LastAction="Libre";
                break;
            case Constants.FIN_SIMULACION:
                LastAction="FIN_SIMULACION";
                break;
        }
    writeTrack();
    }
    private void writeTrack(){
        long curTime = System.currentTimeMillis();
        String trakline= String.valueOf(curTime);

        if(trackGPS) trakline=trakline+", "+longitude+", "+latitude;
        if(trackAcc) trakline=trakline+", "+x+", "+y+", "+z;
        if(trackMag) trakline=trakline+", "+magx+", "+magy+", "+magz;
        if(trackGyro) trakline = trakline+", "+gyrox+", "+gyroy+", "+gyroz;
        if(trackVC) trakline=trakline+", "+LastAction;
        //String Trackline= curTime+", "+longitude+", " +latitude+ ", x " + x + ", y " + y + ", z " + z+", magx " + magx + ", magy " + magy + ", magz " + magz+", "+LastAction;
        Log.i("SensorsReadings: ",trakline);
        try {
            buf.append(trakline);
            buf.newLine();
        } catch (IOException e) {
            e.printStackTrace();
            this.stopSelf();
        }
    }

        @Override
    public void onDestroy(){
            senSensorManager.unregisterListener(this);
            Log.w("onDestroy","destroy");
            try{
                if (buf != null){
                    buf.flush();
                    buf.close();
                    Log.w("onDestroy","buf close");
                }
                if (bufmetadata != null) {
                    bufmetadata.flush();
                    bufmetadata.close();
                    Log.w("onDestroy","bufmetadata close");
                }
            } catch (IOException e) {
                e.printStackTrace();
                this.stopSelf();
            }
        }

}
