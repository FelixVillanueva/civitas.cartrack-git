package com.example.felix.civitascartrack;

/**
 * Created by felix on 6/12/14.
 */
public class Constants {
    public static final int STOP=1;
    public static final int GIRAR_IZQUIERDA =  2;
    public static final int GIRAR_DERECHA =3;
    public static final int ARRANCAR = 4;
    public static final int FRENAR = 5;
    public static final int CARRIL_DERECHA = 6;
    public static final int CARRIL_IZQUIERDA = 7;
    public static final int ROTONDA =8;
    public static final int RECTO =9;
    public static final int ACTIVITY_NOT_RECOGNIZED = -1;
    public static final int FIN_SIMULACION = 10;
    public static final int COCHE = 11;
    public static final int LIBRE = 12;

}
