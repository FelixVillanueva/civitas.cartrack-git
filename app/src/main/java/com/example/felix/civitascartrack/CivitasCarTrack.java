package com.example.felix.civitascartrack;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import java.io.FileOutputStream;
import java.util.ArrayList;


public class CivitasCarTrack extends Activity {
    //private static AcellerometersTracking Accservice;
    Intent mServiceIntent;
    //ComponentName Acc;
    private AcellerometersTracking mBoundService;
    boolean mIsBound;
    ServiceConnection mConnection;

    void doBindService() {
        bindService(new Intent(this,
                AcellerometersTracking.class), mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }
    void doUnbindService() {
        if (mIsBound) {
            // Detach our existing connection.
            unbindService(mConnection);
            mIsBound = false;
        }
    }

    private static final int VOICE_RECOGNITION_REQUEST_CODE = 1234;
    private int get_action(String RecognizedWordArray[]){
        for (int i=0; i<RecognizedWordArray.length; i++){
            if (RecognizedWordArray[i].contains("girar izquierda")) return Constants.GIRAR_IZQUIERDA;
            if (RecognizedWordArray[i].contains("girar derecha")) return Constants.GIRAR_DERECHA;
            if (RecognizedWordArray[i].contains("stop")) return Constants.STOP;
            if (RecognizedWordArray[i].contains("arrancar")) return Constants.ARRANCAR;
            if (RecognizedWordArray[i].contains("frenar")) return Constants.FRENAR;
            if (RecognizedWordArray[i].contains("carril derecha")) return Constants.CARRIL_DERECHA;
            if (RecognizedWordArray[i].contains("carril izquierda")) return Constants.CARRIL_IZQUIERDA;
            if (RecognizedWordArray[i].contains("coche")) return Constants.COCHE;
            if (RecognizedWordArray[i].contains("libre")) return Constants.LIBRE;
            if (RecognizedWordArray[i].contains("rotonda")) return Constants.ROTONDA;
            if (RecognizedWordArray[i].contains("recto")) return Constants.RECTO;
            if (RecognizedWordArray[i].contains("finalizar")) return Constants.FIN_SIMULACION;
            Log.i("CivitasCarTrack", "Words Recogniced " + RecognizedWordArray[i]);
        }
        return Constants.ACTIVITY_NOT_RECOGNIZED;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_civitas_car_track);
        //this.startService(AcellerometersTracking.class);
        mServiceIntent= new Intent(this, AcellerometersTracking.class);
        mConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName className, IBinder service) {
                mBoundService = ((AcellerometersTracking.LocalBinder)service).getService();
            }
            public void onServiceDisconnected(ComponentName className) {
                mBoundService = null;

            }
        };

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_civitas_car_track, menu);
        return true;
    }
    public void stopTracking(View v){
        mServiceIntent = new Intent(this, AcellerometersTracking.class);
        mServiceIntent.putExtra("Reset",true);
        this.stopService(mServiceIntent);
        View bk = findViewById(R.id.background);
        bk.setBackgroundColor(0x808080);
        doUnbindService();

    }

    public void exitCarTrackApp(View v){
        doUnbindService();
        mServiceIntent = new Intent(this, AcellerometersTracking.class);
        this.stopService(mServiceIntent);
        finish();
        return;
    }
    public void startTracking(View v){
        FileOutputStream outputStream;
        View bk = findViewById(R.id.background);
        bk.setBackgroundColor(0xFFEE3333);
        switch (v.getId()){
            case R.id.trackbutton:
                final CheckBox checkBoxGPS = (CheckBox) findViewById(R.id.TrackGPS);
                final CheckBox checkBoxMag = (CheckBox) findViewById(R.id.TrackMag);
                final CheckBox checkBoxAcc = (CheckBox) findViewById(R.id.TrackAcc);
                final CheckBox checkBoxGyro = (CheckBox) findViewById(R.id.TrackGyro);
                final EditText Trackfilename =(EditText) findViewById(R.id.fileName);
                final EditText Comments =(EditText) findViewById(R.id.comment);
                final CheckBox CBvoiceCommands = (CheckBox) findViewById(R.id.voiceCommands);

                mServiceIntent = new Intent(this, AcellerometersTracking.class);
                mServiceIntent.putExtra("TrackMag",checkBoxMag.isChecked());
                mServiceIntent.putExtra("TrackGPS",checkBoxGPS.isChecked());
                mServiceIntent.putExtra("TrackAcc",checkBoxAcc.isChecked());
                mServiceIntent.putExtra("TrackGyro",checkBoxGyro.isChecked());
                mServiceIntent.putExtra("TrackVC",CBvoiceCommands.isChecked());
                mServiceIntent.putExtra("FileName",Trackfilename.getText().toString());
                mServiceIntent.putExtra("Comments",Comments.getText().toString());
                this.startService(mServiceIntent);
                doBindService();
                if (CBvoiceCommands.isChecked()) startVoiceRecognition();
        }
    }
    void startVoiceRecognition() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getClass().getPackage().getName());
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 5);
        startActivityForResult(intent, VOICE_RECOGNITION_REQUEST_CODE);
    }
    @Override
    protected
    void onActivityResult(int requestCode, int resultCode, Intent data) {
        String wordStr[] = null;
        if (requestCode == VOICE_RECOGNITION_REQUEST_CODE && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String RecognizedWordArray[] = matches.toArray(new String[matches.size()]);
            int idAction= get_action(RecognizedWordArray);
            if (idAction==Constants.FIN_SIMULACION){
                mBoundService.VoiceActionNotify(get_action(RecognizedWordArray));
                doUnbindService();
                mServiceIntent = new Intent(this, AcellerometersTracking.class);
                this.stopService(mServiceIntent);
                finish();
                return;
            }
            mBoundService.VoiceActionNotify(idAction);
        }
        // recursive, should i check an efficient way?
        startVoiceRecognition();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onPause(){
        mServiceIntent = new Intent(this, AcellerometersTracking.class);
        this.stopService(mServiceIntent);
        super.onPause();

    }
    @Override
    public void onStop(){
        super.onStop();
    }
    @Override
    public void onDestroy(){
        mServiceIntent = new Intent(this, AcellerometersTracking.class);
        this.stopService(mServiceIntent);
        doUnbindService();
        super.onDestroy();
    }
}
